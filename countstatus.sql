DROP PROCEDURE IF EXISTS CountOrderByStatus;

DELIMITER $$

CREATE PROCEDURE CountOrderByStatus
(IN OrderStatus VARCHAR(50),OUT Total INT)
BEGIN
	select count(orderNumber)
	INTO  Total 
	from orders where status=OrderStatus;
END $$
DELIMITER ;
