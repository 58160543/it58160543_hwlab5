DROP PROCEDURE IF EXISTS getorderByCust;

DELIMITER $$

CREATE PROCEDURE getorderByCust(IN cm INT,out Shipped INT , OUT Cancelled INT, OUt Resolved INT, OUT Disputed INT)
BEGIN
        SELECT count(status)
        INTO Shipped
        from orders
        where customerNumber=cm and status='Shipped';

        Select count(status)
        INTO Cancelled
        from orders
        where customerNumber=cm and status='Cancelled';

        Select count(status)
        INTO Resolved
        from orders
        where customerNumber=cm and status='Resolved';

        Select count(Status)
        INTO Disputed
        from orders
        where customerNumber=cm and status='Disputed';


END $$


DELIMITER ;
