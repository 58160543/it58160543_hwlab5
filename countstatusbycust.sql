DROP PROCEDURE IF EXISTS StatusByCustomer;
DELIMITER $$ 
CREATE PROCEDURE StatusByCustomer
(IN OrderNumber INT,IN CustomerNumber INT ,OUT OrderStatus VARCHAR(50))
BEGIN
	select status
	INTO OrderStatus
	from orders 
	where orderNumber =OrderNumber and 
	customerNumber=CustomerNumber
	LIMIT 1;
END $$

DELIMITER ;
